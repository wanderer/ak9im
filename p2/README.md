# p2

this is a Go subproject containing code for task no. 2.
Python is used for visualisation.

### compile
```sh
go build -v .
```

### run
to compute correlations, impulse function:
```sh
# pass a csv data file.
./p2 -datafile=./data/m.csv
```

to visualise the computed data:
```sh
python visualise.py
```

alternatively, from current folder you could do both of the above in a single
step:
```diff
# compute stuff and visualise.
-./p2 -datafile=./data/m.csv
+./p2 -datafile=./data/m.csv -vis
```
