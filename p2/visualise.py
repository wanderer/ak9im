import seaborn as sb
import pandas as pd
import scipy
import matplotlib.pyplot as plt

data_uy=pd.read_csv('data/m.csv')
#  head(data_uy)

correlation_result=scipy.stats.pearsonr(data_uy['u'], data_uy['y'])
print(correlation_result)

p = sb.lmplot(
		data=data_uy,
		x='u',
		y='y',
		fit_reg=True,
		height=8,
		order=2,
	)
p.set(title='Correlation UY')
p.savefig('res/uy-correlation-analysis.png')
plt.clf()

lp = sb.lineplot(data=data_uy['u'])
lp.set(
		title='Signal u',
		xlabel='kT',
		ylabel='u',
	)
fig = lp.get_figure()
fig.subplots_adjust(top=.97)
fig.savefig('res/signal_u.png')
plt.clf()

lp = sb.lineplot(data=data_uy['y'])
lp.set(
		title='Signal y',
		xlabel='kT',
		ylabel='y',
	)
fig = lp.get_figure()
fig.subplots_adjust(top=.97)
fig.savefig('res/signal_y.png')
plt.clf()

imf=pd.read_csv('data/impulse_func.csv')
lp = sb.lineplot(data=imf, legend=False, linewidth=2.5)
lp.set(
		title='Impulse Function Estimate (gCustom)',
		xlabel='time (s)',
		ylabel='amplitude',
	)
fig = lp.get_figure()
fig.subplots_adjust(top=.97)
fig.savefig('res/impulse_func_estimate.png')
plt.clf()

acr=pd.read_csv('data/autocorrelation_u.csv')
lp = sb.lineplot(data=acr, legend=False, linewidth=2.5)
lp.set(
		title='Autocorrelation of u (Ruu)',
		xlabel='shift',
		ylabel='Ruu',
	)
fig = lp.get_figure()
fig.subplots_adjust(top=.97)
fig.savefig('res/autocorrelation_u.png')
plt.clf()

acr=pd.read_csv('data/autocorrelation_y.csv')
lp = sb.lineplot(data=acr, legend=False, linewidth=2.5)
lp.set(
		title='Autocorrelation of y (Ryy)',
		xlabel='shift',
		ylabel='Ryy',
	)
fig = lp.get_figure()
fig.subplots_adjust(top=.97)
fig.savefig('res/autocorrelation_y.png')
plt.clf()

mcr=pd.read_csv('data/mutual_correlation_uy.csv')
lp = sb.lineplot(data=mcr, legend=False, linewidth=2.5)
lp.set(
		title='Mutual correlation of uy (Ruy)',
		xlabel='shift',
		ylabel='Ruy',
	)
fig = lp.get_figure()
fig.subplots_adjust(top=.97)
fig.savefig('res/mutual_correlation_uy.png')
plt.clf()

mcr=pd.read_csv('data/mutual_correlation_yu.csv')
lp = sb.lineplot(data=mcr, legend=False, linewidth=2.5)
lp.set(
		title='Mutual correlation of yu (Ryu)',
		xlabel='shift',
		ylabel='Ryu',
	)
fig = lp.get_figure()
fig.subplots_adjust(top=.97)
fig.savefig('res/mutual_correlation_yu.png')
plt.clf()

# plot comparison
imf=pd.read_csv('data/impulse_func.csv')
imfM=pd.read_csv('data/ircra.csv')

plt.plot(range(0, len(imf)), imf, label='gCustom')
plt.plot(range(0, len(imfM)), imfM, label='gSIT')
plt.legend(loc="upper right")
plt.title('Impulse Function Estimate Comparison')
plt.xlabel('time (s)')
plt.ylabel('amplitude')
plt.savefig('res/impulse_func_estimate_comparison.png')
plt.close()
