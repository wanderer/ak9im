package stats

import "testing"

func TestAutocorrelate(t *testing.T) {
	testCases := []struct {
		desc     string
		maxShift float64
		in       []float64
		out      []float64
	}{
		{
			desc:     "autocorrelate a 10-long slice",
			maxShift: .1,
			in:       []float64{.4, .5, .23, 2.5, -3.6, .4, -0.12, 2.2, 0.02, 14.4},
			out:      []float64{23.204770000000003, 1.0588888888888885},
		},
		{
			desc:     "autocorrelate a 20-long slice",
			maxShift: .1,
			in:       []float64{.4, .5, .23, 2.5, -3.6, .4, -0.12, 2.2, 0.02, 14.4, .4, .5, .23, 2.5, -3.6, .4, -0.12, 2.2, 0.02, 14.4},
			out:      []float64{23.204770000000003, 0.6999999999999996, 4.234177777777778},
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			want := tC.out
			got := Autocorrelate(tC.in, tC.maxShift)

			if len(want) != len(got) {
				t.Errorf("Slices are of different sizes, want: %d, got: %d",
					len(want), len(got),
				)
			}

			for i := range want {
				if want[i] != got[i] {
					t.Errorf("Unexpected output for %q: expected %+v, actual %+v",
						tC.desc, want, got,
					)
				}
			}
		})
	}
}

func BenchmarkAutocorrelate(b *testing.B) {
	// run the function b.N times.
	for n := 0; n < b.N; n++ {
		Autocorrelate([]float64{.4, .5, .23, 2.5, -3.6, .4, -0.12, 2.2, 0.02, 14.4}, .1)
	}
}

func BenchmarkAutocorrelateMP(b *testing.B) {
	// run the function b.N times.
	for n := 0; n < b.N; n++ {
		AutocorrelateMP([]float64{.4, .5, .23, 2.5, -3.6, .4, -0.12, 2.2, 0.02, 14.4}, .1)
	}
}
