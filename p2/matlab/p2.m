cd ~/src/utb/ak9im/ak9im/p2;

%% load model
load('data/task2.slx');

step(1, [2 5 1])
%% run the simulation in simulink
length(out.u);
length(out.y);

cd ~/src/utb/ak9im/ak9im/p2/data/;

m = [out.u, out.y];
% writematrix(m, 'data/m.csv')

%% define a system from the transfer function
sys = tf(1, [2 5 1])
%% get impulse response from CST
ir = impulse(sys)
%% get impulse response from SIT
ircra = cra(out.u, out.y)

%% save CST impulse response to file
% writematrix(ir, 'data/ir.csv')
%% save SIT impulse response to file
% writematrix(ircra, 'data/ircra.csv')

