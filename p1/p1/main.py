import sys
import getopt
import numpy as np
import funcs as f


def main(argv):
	inputfile = ''
	try:
		opts, args = getopt.getopt(argv, "hi:o:", ["ifile="])
	except getopt.GetoptError:
		print('main.py -i <inputfile>')
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print('main.py -i <inputfile>')
			sys.exit()
		elif opt in ("-i", "--ifile"):
			inputfile = arg
	if inputfile != '':
		print('Input file is', inputfile)
	else:
		print('no input file provided, see help (-h)\nexiting...')
		sys.exit(1)

	#  d = f.load_d("./dat.csv")
	d = f.load_d(inputfile)
	# d = d.astype('float64')
	print(d.head(), '\n')

	# alternative colour used to differentiate from the default.
	altcol = '#ff7f0e'

	# data plots
	du = d['u']
	dy = d['y']
	f.plot_d(du, fname='u')
	f.plot_d(dy, fname='y', colour=altcol)

	# mean and variance
	mean_u = f.mean(d['u'])
	mean_y = f.mean(d['y'])
	print('mean u:', mean_u)
	print('mean y:', mean_y)

	variance_u = f.variance(d['u'])
	variance_y = f.variance(d['y'])
	print('variance u:', variance_u)
	print('variance y:', variance_y)

	f.histogram(d['u'], fname='u')
	f.histogram(d['y'], fname='y', colour=altcol)

	f.distr_func(d['u'], fname='u')
	f.distr_func(d['y'], fname='y', colour=altcol)

	cov = f.covar(d)
	std_dev_u = f.std_dev(d['u'])
	std_dev_y = f.std_dev(d['y'])
	# correlation coefficient
	rUY = f.correlation_coefficient(cov, std_dev_u, std_dev_y)
	print("correlation coefficient rUY:", rUY)

	print("covariance matrix (built-in):\n", d.cov(), '\n')
	# print the covariance matrix.
	print("covariance matrix (own):")
	print(np.array([[variance_u, cov], [cov, variance_y]]))

	f.plot_autocorrelation(dat=d['u'], fname='u')
	f.plot_autocorrelation(dat=d['y'], fname='y', colour=altcol)

	f.plot_mutual_correlation(d1=d['u'], d2=d['y'], fname='uy')
	f.plot_mutual_correlation(d1=d['y'], d2=d['u'], fname='yu')

	f.plot_autocovariance(dat=d['u'], fname='u')
	f.plot_autocovariance(dat=d['y'], fname='y', colour=altcol)

	#  we don't need this atm.
	#  f.plot_mutual_covariance(d1=d['u'], d2=d['y'], fname='uy')
	#  f.plot_mutual_covariance(d1=d['y'], d2=d['u'], fname='yu')


if __name__ == "__main__":
	main(sys.argv[1:])
