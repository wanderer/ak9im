import pandas as pd
import numpy as np
import matplotlib.pyplot as pyplt


def plt_ticks_size() -> int:
	return 10


def load_d(path: str) -> pd.DataFrame():
	return pd.read_csv(path, float_precision='round_trip', dtype='float64')


def plot_d(dat: pd.Series, fname: str = 'x', colour: str = '#1f77b4'):
	pyplt.xlim(0, len(dat))
	pyplt.ylim(min(dat) - 0.3, max(dat) + 0.3)
	pyplt.xticks(size=plt_ticks_size())
	pyplt.yticks(size=plt_ticks_size())
	pyplt.title('Signal ' + fname)
	pyplt.plot(dat, color=colour)
	pyplt.savefig('signal_' + fname + '.jpg')
	# pplt.show(block=0)
	# rework this
	# ref: https://stackoverflow.com/a/46418284
	pyplt.close()


def mean(dat: pd.Series) -> float:
	return dat.sum() / len(dat)


def variance(dat: pd.Series) -> float:
	return sum(pow(dat - mean(dat), 2)) / len(dat)


def histogram(dat: pd.Series, bins: int = 10, fname: str = 'x', colour: str = '#1f77b4'):
	pyplt.title('Histogram of ' + fname)
	pyplt.xlabel(fname)
	pyplt.ylabel('probability')

	pyplt.hist(dat, color=colour, bins=bins, edgecolor='black')
	pyplt.xticks(size=plt_ticks_size())
	pyplt.yticks(size=plt_ticks_size())
	pyplt.savefig('hist_' + fname + '.jpg')
	pyplt.close()


def distr_func(dat: pd.Series, bins: int = 20, fname: str = 'x', colour: str = '#1f77b4'):
	N = len(dat)/bins
	x = np.sort(dat)[::bins]
	y = np.arange(1, N+1) / float(N)

	pyplt.xlabel(fname)
	pyplt.ylabel('probability')
	pyplt.title('Cumulative Distribution Function of ' + fname)
	pyplt.hist(dat, bins=bins, cumulative=True, density=1, color=colour)
	pyplt.savefig('cdf_' + fname + '.jpg')
	pyplt.close()


def std_dev(dat: pd.Series) -> float:
	return np.sqrt(variance(dat))


def mean_diff(dat: pd.DataFrame) -> pd.Series:
	return dat - dat.mean()


def covar(dat: pd.DataFrame) -> float:
	dat = dat.apply(mean_diff)
	return dat['u'].dot(dat['y']) / len(dat)


def correlation_coefficient(cov: float, std_dev_u: float, std_dev_y: float) -> float:
	return cov / (std_dev_u * std_dev_y)


def auto_covariance(dat: list, max_shift_n: int = .1) -> list:
	v = []
	# m is the max permissible shift value.
	m = len(dat) * max_shift_n
	mean = np.mean(dat)
	cur_shift = 0

	while m >= cur_shift:
		r = 0

		for i in range(len(dat) - cur_shift):
			r += (dat[i] - mean) * (dat[i + cur_shift] - mean)

		r = r * (1 / len(dat) - cur_shift)
		v.append(r)
		cur_shift += 1

	return v


def mutual_correlation(dx: list, dy: list, max_shift_n: int = .1) -> list:
	v = []
	m = len(dx) * max_shift_n
	cur_shift = 0

	while m >= cur_shift:
		r = 0

		for i in range(len(dx) - cur_shift):
			r += dx[i] * dy[i + cur_shift]

		r = r * (1 / (len(dx) - cur_shift))
		v.append(r)
		cur_shift += 1

	return v


def auto_correlation(dat: list, max_shift_n: int = .1) -> list:
	v = []
	m = len(dat) * max_shift_n
	cur_shift = 0

	while m >= cur_shift:
		r = 0

		for i in range(len(dat) - cur_shift):
			r += dat[i] * dat[i + cur_shift]

		r = r * (1 / (len(dat) - cur_shift))
		v.append(r)
		cur_shift += 1

	return v


def mutual_covar(dx: list, dy: list, max_shift_n: int = .1) -> list:
	v = []
	m = len(dx) * max_shift_n
	mean_x, mean_y = np.mean(dx), np.mean(dy)
	cur_shift = 0

	while m >= cur_shift:
		r = 0

		for i in range(len(dx) - cur_shift):
			r += (dx[i] - mean_x) * (dy[i + cur_shift] - mean_y)

		r = r / (len(dx) - 1 - cur_shift)
		v.append(r)
		cur_shift += 1

	return v


def plot_autocovariance(dat: pd.DataFrame, fname: str = 'x', colour: str = '#1f77b4'):
	d = auto_covariance(dat.tolist())
	pyplt.plot(range(0, len(d)), d, color=colour)
	pyplt.title('Autocovariance of ' + fname + ' (C' + fname + fname + ')')
	pyplt.xlabel('shift')
	pyplt.ylabel('C' + fname + fname)
	pyplt.savefig('autocovariance_' + fname + '.jpg')
	pyplt.close()


def plot_mutual_correlation(d1: pd.DataFrame, d2: pd.DataFrame, fname: str = 'xy', colour: str = '#1f77b4'):
	d = mutual_correlation(d1.tolist(), d2.tolist())
	pyplt.plot(range(0, len(d)), d, color=colour)
	pyplt.title('Mutual correlation of ' + fname + ' (R' + fname + ')')
	pyplt.xlabel('shift')
	pyplt.ylabel('R' + fname)
	pyplt.savefig('mutual_correlation_' + fname + '.jpg')
	pyplt.close()


def plot_autocorrelation(dat: pd.DataFrame, fname: str = 'x', colour: str = '#1f77b4'):
	d = auto_correlation(dat.tolist())
	# exm. TODO: check against matlab.
	# z = pd.Series([.4, .5, .23, 2.5, -3.6, .4, -0.12, 2.2, 0.02, 14.4])
	# a = auto_correlation(z.tolist())
	# print(a)
	pyplt.plot(range(0, len(d)), d, color=colour)
	pyplt.title('Autocorrelation of ' + fname + ' (R' + fname + fname + ')')
	pyplt.xlabel('shift')
	pyplt.ylabel('R' + fname + fname)
	pyplt.savefig('autocorrelation_' + fname + '.jpg')
	pyplt.close()


def plot_mutual_covariance(d1: pd.DataFrame, d2: pd.DataFrame, fname: str = 'xy', colour: str = '#1f77b4'):
	d = mutual_covar(d1.tolist(), d2.tolist())
	#  pyplt.scatter(range(0, len(d)), d, color=colour)
	pyplt.plot(range(0, len(d)), d, color=colour)
	pyplt.title('Mutual covariance of ' + fname + '(C' + fname + fname + ')')
	pyplt.xlabel('shift')
	pyplt.ylabel('C' + fname)
	pyplt.savefig('mutual_covariance_' + fname + '.jpg')
	pyplt.close()
