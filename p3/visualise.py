import pandas as pd
import numpy as np
import seaborn as sb
import scipy
import matplotlib.pyplot as plt

uy = pd.read_csv('data/m.csv')
print(uy)
u = uy['u']
u = u[1:len(u)]
y = uy['y']
y = y[1:len(y)]


plt.plot(u)
plt.xlabel("$ \mathit{k}T $")
plt.title("Signal u")
plt.savefig('res/signal_u.png', dpi=300)
plt.clf()
# ----
plt.plot(y)
plt.xlabel("$ \mathit{k}T $")
plt.title("Signal y")
plt.savefig('res/signal_y.png', dpi=300)
plt.clf()

correlation=scipy.stats.pearsonr(uy['u'], uy['y'])
print(correlation)

p = sb.lmplot(
		data=uy,
		x='u',
		y='y',
		fit_reg=True,
		order=2,
	)
p.set(title='Correlation UY')
p.savefig('res/uy_correlation.png', dpi=300)
plt.clf()
plt.close()


theta = pd.read_csv('data/theta.csv', header=None)
#  theta = np.transpose(theta) # -> no need to transpose.

th_columns = ["$\hat{a}_1 $", "$\hat{a}_2 $", "$\hat{b}_1$", "$\hat{b}_2$"]
for i in theta:
	plt.plot(theta[i], label=th_columns[i])
	plt.locator_params(axis='x', nbins=12)

plt.legend()
plt.xlabel("$\mathit{k}T$")
plt.title("ARX Theta (θ) Parameter Estimation using RLSq")
plt.savefig('res/theta.png', dpi=300)
plt.clf()


error = pd.read_csv('data/estimate_error.csv', header=None)

plt.plot(error, label="$\hat{e}(\mathit{k}T)$")
plt.locator_params(axis='x', nbins=12)
plt.legend()
plt.xlabel("$\mathit{k}T$")
plt.title("Error of ARX Theta (θ) Parameter Estimation using RLSq")
plt.savefig('res/error.png', dpi=300)
plt.clf()


explicitTheta = pd.read_csv('data/explicit_theta.csv', header=None)

eTx = explicitTheta[0:4]
eTy = explicitTheta[4:8]
eTxy = [np.transpose(eTx), np.transpose(eTy)]
plt.scatter(th_columns, eTxy[0], label="$\hat{θ}_0$")
plt.scatter(th_columns, eTxy[1], label="$\hat{θ}_1$")
plt.legend()
plt.title("ARX Theta (θ) Parameter Estimation using ELSq")
plt.savefig('res/explicit_theta.png', dpi=300)
plt.clf()


explicitError = pd.read_csv('data/explicit_error.csv', header=None)

plt.plot(explicitError, label="$ê(\mathit{k}T)$")
plt.locator_params(axis='x', nbins=12)
plt.legend()
plt.xlabel("$\mathit{k}T$")
plt.title("Error of ARX Theta (θ) Parameter Estimation using ELSq")
plt.savefig('res/explicit_error.png', dpi=300)
plt.clf()
