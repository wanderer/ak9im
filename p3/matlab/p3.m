cd ~/src/utb/ak9im/ak9im/p3;

%% load model
%load('data/task3.slx');
%load("data/u.csv");


step(1, [2 5 1])

%% run the simulation in simulink
len_u = length(u);
len_y = length(y);

%% save data
% cd ~/src/utb/ak9im/ak9im/p2/data/;
m = [out.u, out.y];
% writematrix(m, 'data/m.csv')

%% plot input/output signals.
plot([1:len_u],out.u)
plot([1:len_y],out.y)

[n d] = c2dm(1, [2 5 1], 1);
