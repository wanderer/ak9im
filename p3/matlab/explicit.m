pwd;

% load data.
load('../data/u.csv');
load('../data/y.csv');

% number of params of the ARX model we're estimating.
p = 4;

% create toeplitz matrices from the data.
fu = toeplitz(u);
fy = toeplitz(y);

% construct a sub-matrix from the toeplitz matrices.
F = [fy(p:end-1,1:p), fu(p:end-1,1:p)];

yy = y(p+1:end);

% get the parameters estimate, with F' being the transpose of F.
theta = (F' * F) \ (F' * yy);

% calculate the estimate error.
err = yy - (F * theta);

% save stuff.
writematrix(theta, '../data/explicit_theta.csv');
writematrix(err, '../data/explicit_error.csv');

exit;
