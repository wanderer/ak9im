// Package lrls provides functions for calculating linear regression using the
// 'Least Squares' method, resp its variations: explicit and recursive.
package lrls
