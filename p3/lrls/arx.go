package lrls

import (
	"errors"
	"log"
)

const (
	methodExplicit = iota
	methodRecursive
)

// Estimate runs the selected method to estimate parameters of ARX model,
// returns them and the error of the estimate. valid estimation method values
// are 0 for explicit and 1 for recursive.
// the function returns [][]float64 of parameter estimates, ([]float64 of error
// values, nil for error) on happy path, (nil, nil, err) otherwise.
func Estimate(eMethod int, u, y []float64) ([][]float64, []float64, error) {
	if eMethod < 0 || eMethod > 1 {
		log.Printf("error: estimation method needs to be from the set {0, 1}, got: %d", eMethod)

		return nil, nil, errors.New("ErrInvalidMethod")
	}

	method := eMethod
	theta := make([][]float64, 0)
	errs := make([]float64, 0)

	var err error

	switch method {
	case methodExplicit:
		err = explicit()

	case methodRecursive:
		theta, errs, err = recursive(u, y)
	}

	if err != nil {
		return nil, nil, err
	}

	return theta, errs, nil
}
