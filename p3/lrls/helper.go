package lrls

import (
	"errors"
	"log"

	"gonum.org/v1/gonum/mat"
)

// diagMat returns a Diagonal Matrix initialised with *data* across the main
// diagonal.
// nolint: unused
func diagMat(data []float64) *mat.DiagDense {
	n := len(data)

	dm := mat.NewDiagDense(n, data)

	return dm
}

// nolint: unused
func diagVals(n int) []float64 {
	d := make([]float64, n)

	for i := 0; i < n; i++ {
		d[i] = 1000000
	}

	return d
}

// nolint: unused
func covMatrices(size, params int) ([]*mat.Dense, error) {
	switch {
	case size < 1:
		return nil, errors.New("ErrCovMatricesSize")

	case params < 4 || params > 5:
		log.Printf("error: params needs to be from the set {4 ,5}, got: %d", params)
		return nil, errors.New("ErrBadParams")
	}

	// slice of covariance matrices.
	cms := make([]*mat.Dense, size)

	for i := range cms {
		cms[i] = mat.NewDense(params, params, nil)
	}

	// create a matrix of the same shape as the cov matrix, with *diagVals*
	// along the main diagonal.
	dm := diagMat(diagVals(params))

	// replace the fist 3 covariance matrices with the diagonal matrix.
	for i := 0; i < 4; i++ {
		cms[i].CloneFrom(dm)
	}

	return cms, nil
}

// nolint: unused
func newTheta(size, params int) ([][]float64, error) {
	switch {
	case size < 1:
		return nil, errors.New("ErrThetaSize")

	case params < 4 || params > 5:
		log.Printf("error: params needs to be from the set {4 ,5}, got: %d", params)
		return nil, errors.New("ErrBadParams")
	}

	th := make([][]float64, size)

	// initialise.
	for i := range th {
		th[i] = make([]float64, params)
	}

	// specifically set the second vector to all 0.1s.
	for i := range th[1] {
		th[1][i] = 0.1
	}

	return th, nil
}

// nolint: unused
func newPhi(size, params int) ([][]float64, error) {
	switch {
	case size < 1:
		return nil, errors.New("ErrFiSize")

	case params < 4 || params > 5:
		log.Printf("error: params needs to be from the set {4 ,5}, got: %d", params)
		return nil, errors.New("ErrBadParams")
	}

	phi := make([][]float64, size)

	// prealloc.
	for i := range phi {
		phi[i] = make([]float64, params)
	}

	return phi, nil
}
