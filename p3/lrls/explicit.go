package lrls

import (
	"log"
	"os/exec"
)

func explicit() error {
	err := runExplicitMatlab()
	if err != nil {
		return err
	}

	// TODO: read data processed and saved by matlab and return that.

	return nil
}

func runExplicitMatlab() error {
	red := "\033[31m"
	reset := "\033[0m"
	cmd := exec.Command("matlab", "-batch", "explicit")

	cmd.Dir = "./matlab"

	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Printf("running matlab failed with:\n\n%s%s%s\n", red, out, reset)
		return err
	}

	return nil
}
